#include "ns3/boolean.h"
#include "ns3/double.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/log.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/single-model-spectrum-channel.h"
#include <ns3/channel.h>
#include <ns3/mobility-model.h>
#include <ns3/nstime.h>
#include <ns3/object.h>
#include <ns3/phased-array-spectrum-propagation-loss-model.h>
#include <ns3/propagation-delay-model.h>
#include <ns3/propagation-loss-model.h>
#include <ns3/spectrum-phy.h>
#include <ns3/spectrum-propagation-loss-model.h>
#include <ns3/spectrum-signal-parameters.h>
#include <ns3/traced-callback.h>
#include "ns3/non-communicating-net-device.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include <chrono>
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/interference-helper.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/object.h"
#include "ns3/random-variable-stream.h"
#include <map>
#include <iostream>
#include "ns3/ptr.h"
#include "ns3/waveform-generator-helper.h"
#include "ns3/waveform-generator.h"
#include "ns3/wifi-net-device.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/uinteger.h"
#include "ns3/waveform-generator-helper.h"
#include "ns3/waveform-generator.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/interference-helper.h"
#include <iomanip>
#include <ns3/spectrum-channel.h>
#include <ns3/spectrum-model.h>
#include <ns3/traced-callback.h>

using namespace ns3;
const double SpeedOfLight = 299792458.0; 



NS_LOG_COMPONENT_DEFINE("SecondProject");

Ptr<SpectrumModel> SpectrumModelWifi5180MHz; 
Ptr<SpectrumModel> SpectrumModelWifi5190MHz;


class static_SpectrumModelWifi5180MHz_initializer
{
  public:
    static_SpectrumModelWifi5180MHz_initializer()
    {
        BandInfo bandInfo;
        bandInfo.fc = 5180e6;
        bandInfo.fl = 5180e6 - 10e6;
        bandInfo.fh = 5180e6 + 10e6;

        Bands bands;
        bands.push_back(bandInfo);

        SpectrumModelWifi5180MHz = Create<SpectrumModel>(bands);
    }
};

static_SpectrumModelWifi5180MHz_initializer static_SpectrumModelWifi5180MHz_initializer_instance;
int main(int argc, char* argv[])
{   
double distance = 10.0; // m
bool udp = false;
double simulationTime = 10; // seconds
double intervalTime = 1;
std::string wifiType = "ns3::YansWifiPhy";
std::string errorModelType = "ns3::NistErrorRateModel";

CommandLine cmd(__FILE__);
cmd.AddValue("udp", "UDP if set to 1, TCP otherwise", udp);
cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
cmd.AddValue("distance", "meters separation between nodes", distance);
cmd.AddValue("wifiType", "select ns3::SpectrumWifiPhy or ns3::YansWifiPhy", wifiType);
cmd.AddValue("errorModelType",
"select ns3::NistErrorRateModel or ns3::YansErrorRateModel",
errorModelType);
cmd.Parse(argc, argv);
uint32_t payloadSize;
payloadSize = 972;
uint16_t frequency = 5180;

NodeContainer wifiStaNode;
wifiStaNode.Create(2);
NodeContainer wifiApNode;
wifiApNode.Create(2);
YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
YansWifiPhyHelper phy;
phy.SetChannel(channel.Create());
SpectrumWifiPhyHelper spectrumPhy;
WifiHelper wifi;
wifi.SetStandard(WIFI_STANDARD_80211n);
NetDeviceContainer apDevice;
NetDeviceContainer apDevice2;
NetDeviceContainer staDevice;
NetDeviceContainer staDevice2;

Ssid ssid = Ssid("ns380211n");
WifiMacHelper mac;

Ptr<MultiModelSpectrumChannel> spectrumChannel;




if (wifiType == "ns3::YansWifiPhy")
{
    ssid = Ssid("network-A");
    phy.Set("ChannelSettings", StringValue("{36, 20, BAND_5GHZ, 0}"));
    mac.SetType("ns3::StaWifiMac", "QosSupported", BooleanValue(true), "Ssid", SsidValue(ssid));
    staDevice = wifi.Install(phy, mac, wifiStaNode.Get(0));

    mac.SetType("ns3::ApWifiMac",
                "QosSupported",
                BooleanValue(true),
                "Ssid",
                SsidValue(ssid),
                "EnableBeaconJitter",
                BooleanValue(false));
    apDevice = wifi.Install(phy, mac, wifiApNode.Get(0));


    ssid = Ssid("network-B");
    phy.Set("ChannelSettings", StringValue("{40, 20, BAND_5GHZ, 0}"));
    mac.SetType("ns3::StaWifiMac", "QosSupported", BooleanValue(true), "Ssid", SsidValue(ssid));
    staDevice2 = wifi.Install(phy, mac, wifiStaNode.Get(1));

    mac.SetType("ns3::ApWifiMac",
                "QosSupported",
                BooleanValue(true),
                "Ssid",
                SsidValue(ssid),
                "EnableBeaconJitter",
                BooleanValue(false));
    apDevice2 = wifi.Install(phy, mac, wifiApNode.Get(1));
}
else if (wifiType == "ns3::SpectrumWifiPhy")
{ 
  
spectrumChannel = CreateObject<MultiModelSpectrumChannel>();
Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
lossModel->SetFrequency(5180 * 1e6);
spectrumChannel->AddPropagationLossModel(lossModel);
Ptr<ConstantSpeedPropagationDelayModel> delayModel = CreateObject<ConstantSpeedPropagationDelayModel>();
spectrumChannel->SetPropagationDelayModel(delayModel);
spectrumPhy.SetChannel(spectrumChannel);
spectrumPhy.SetErrorRateModel(errorModelType);
spectrumPhy.Set("ChannelSettings",
                                StringValue(std::string("{") + (frequency == 5180 ? "36" : "38") +
                                ", 0, BAND_5GHZ, 0}"));

} 

else {
    NS_FATAL_ERROR("Unsupported WiFi type " << wifiType);
}





if (wifiType == "ns3::SpectrumWifiPhy")
{
  WifiMacHelper apMac;
  apMac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid));

  WifiMacHelper staMac;
  staMac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid));
      staDevice = wifi.Install(spectrumPhy, staMac,wifiStaNode.Get(0));
      apDevice = wifi.Install(spectrumPhy, apMac,wifiApNode.Get(0));
      staDevice2 = wifi.Install(spectrumPhy, staMac,wifiStaNode.Get(1));
      apDevice2 = wifi.Install(spectrumPhy, apMac,wifiApNode.Get(1));
}


MobilityHelper mobility;
Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();

positionAlloc->Add(Vector(0.0, 0.0, 0.0));
positionAlloc->Add(Vector(distance, 0.0, 0.0));
positionAlloc->Add(Vector(0, distance/2, 0.0)); 
positionAlloc->Add(Vector(distance, -distance/2, 0.0)); 
mobility.SetPositionAllocator(positionAlloc);

mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");

mobility.Install(wifiApNode.Get(0));
mobility.Install(wifiApNode.Get(1));
mobility.Install(wifiStaNode.Get(0));
mobility.Install(wifiStaNode.Get(1));

InternetStackHelper stack;
stack.Install(wifiApNode.Get(0));
stack.Install(wifiApNode.Get(1));
stack.Install(wifiStaNode.Get(0));
stack.Install(wifiStaNode.Get(1));

Ipv4AddressHelper address;
address.SetBase("192.168.1.0", "255.255.255.0");
Ipv4InterfaceContainer apNodeInterface;
Ipv4InterfaceContainer staNodeInterface;
apNodeInterface = address.Assign(apDevice);
staNodeInterface = address.Assign(staDevice);


Ipv4AddressHelper address2;
address2.SetBase("192.168.2.0", "255.255.255.0");
Ipv4InterfaceContainer apNodeInterface2;
Ipv4InterfaceContainer staNodeInterface2;
apNodeInterface2 = address2.Assign(apDevice2);
staNodeInterface2 = address2.Assign(staDevice2);


if (udp)
        { ApplicationContainer serverApp;
UdpServerHelper server(9);
serverApp = server.Install(wifiStaNode.Get(0));
serverApp.Start(Seconds(0.0));


ApplicationContainer serverApp2;
UdpServerHelper server2(10);
serverApp2 = server2.Install(wifiStaNode.Get(1));
serverApp2.Start(Seconds(0.0));

UdpClientHelper client(staNodeInterface.GetAddress(0), 9);
client.SetAttribute("MaxPackets", UintegerValue(4294967295U));
client.SetAttribute("Interval", TimeValue(Time("0.00001"))); // packets/s
client.SetAttribute("PacketSize", UintegerValue(payloadSize));
ApplicationContainer clientApp = client.Install(wifiApNode.Get(0));
clientApp.Start(Seconds(0.0));

UdpClientHelper client2(staNodeInterface2.GetAddress(0), 10);
client2.SetAttribute("MaxPackets", UintegerValue(4294967295U));
client2.SetAttribute("Interval", TimeValue(Time("0.00001"))); // packets/s
client2.SetAttribute("PacketSize", UintegerValue(payloadSize));
ApplicationContainer clientApp2 = client2.Install(wifiApNode.Get(1));
clientApp2.Start(Seconds(0.0));}
else if (udp == false)
{  uint32_t payloadSizetcp;
    payloadSizetcp = 1448; // 1500 bytes IPv6
    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSizetcp));
    
    ApplicationContainer serverApp;
    uint16_t port = 50000;
    Address localAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
    PacketSinkHelper packetSinkHelper("ns3::TcpSocketFactory", localAddress);
    serverApp = packetSinkHelper.Install(wifiStaNode.Get(0));
    serverApp.Start(Seconds(0.0));
      

    OnOffHelper onoff("ns3::TcpSocketFactory", Ipv4Address::GetAny());
    onoff.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    onoff.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    onoff.SetAttribute("PacketSize", UintegerValue(payloadSizetcp));
    onoff.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
    AddressValue remoteAddress(InetSocketAddress(staNodeInterface.GetAddress(0), port));
    onoff.SetAttribute("Remote", remoteAddress);
    ApplicationContainer clientApp = onoff.Install(wifiApNode.Get(0));
    clientApp.Start(Seconds(0.0));


    ApplicationContainer serverApp2;
    uint16_t port2 = 50001;
    Address localAddress2(InetSocketAddress(Ipv4Address::GetAny(), port2));
    PacketSinkHelper packetSinkHelper2("ns3::TcpSocketFactory", localAddress2);
    serverApp2 = packetSinkHelper2.Install(wifiStaNode.Get(1));
       

    OnOffHelper onoff2("ns3::TcpSocketFactory", Ipv4Address::GetAny());
    onoff2.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    onoff2.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    onoff2.SetAttribute("PacketSize", UintegerValue(payloadSizetcp));
    onoff2.SetAttribute("DataRate", DataRateValue(1000000000)); // bit/s
    AddressValue remoteAddress2(InetSocketAddress(staNodeInterface2.GetAddress(0), port2));
    onoff2.SetAttribute("Remote", remoteAddress2);
    ApplicationContainer clientApp2 = onoff2.Install(wifiApNode.Get(1));
    clientApp2.Start(Seconds(0.0));
}




Ptr<FlowMonitor> flowMonitor;
FlowMonitorHelper flowHelper;
flowMonitor = flowHelper.InstallAll();



Simulator::Stop(Seconds(simulationTime + 1));
auto start = std::chrono::steady_clock::now();
Simulator::Run();




flowMonitor->CheckForLostPackets(); // Controllo dei pacchetti persi
Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier>(flowHelper.GetClassifier());
FlowMonitor::FlowStatsContainer stats = flowMonitor->GetFlowStats();



 double totalThroughput = 0.0;
  int numFlows = 0;
for (auto it = stats.begin(); it != stats.end(); ++it) {
  Ipv4FlowClassifier::FiveTuple tuple = classifier->FindFlow(it->first);

  std::cout << "Flow ID: " << it->first << " Src Addr: " << tuple.sourceAddress << " Dst Addr: " << tuple.destinationAddress << "\n";
  std::cout << "  Tx Packets: " << it->second.txPackets << "\n";
  std::cout << "  Rx Packets: " << it->second.rxPackets << "\n";
  std::cout << "  Delay: " << it->second.delaySum.GetSeconds() / it->second.rxPackets << " seconds\n";


  for (double t = intervalTime; t <= 50; t += intervalTime) {
    double throughput = it->second.rxBytes * 8.0 / t /1024 /1024;
    std::cout << "  Throughput at " << t << " seconds: " << throughput << " Mbps\n";
   
totalThroughput += throughput;
numFlows++;}
 double averageThroughput = totalThroughput / numFlows;
  std::cout << "Average Throughput: " << averageThroughput << " Mbps\n";
}

 
  auto end = std::chrono::steady_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
  std::cout << "Tempo impiegato dalla simulazione: " << duration.count() << " millisecondi" << std::endl;
  ns3::Simulator::Destroy();
Simulator::Destroy();

return 0;
}
